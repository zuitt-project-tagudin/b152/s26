/*
	Client-Server Architecture is a copmuting model wherein a server hosts, delivers and manages resources that a client consumes.

	What is a client?
		-a client is an application which creates requests for resources from a server. a client will trigger an action, through a url, and wait for the response of the server.

	What is a server?
		- a server is able to host and deliver resources requested by a client. a single server can handle multiple clients.

	Node.js is an encironment to be able to develop applications with Javascript. with this, we can run JS with out HTML.

	JS can now be used to create backend applications.

	
	Front-End	usually  the page that users see; usually a client 				application that requests resources from a server or a 				backend
	Back-End	usually server-related applications which handle 				requests from a front-end client

	
	Why is NodeJS popular?
		Performance
		Familiarity
		NPM - Node Package Manager


*/

//console.log("Hello, World!");



/*
	require() method to load node.js modules.
		a module is a software component or part of a program which contains one or more routines.

		the http module is a default module from node.js
			the module let nodejs transfer data, let the client and server exchange data via Hypertext Transfer Protocol (http)

			protocol => http://localhost:4000 <= server

			the client (browser) automatically created a request and our server was able to respond with a message/

*/			

/*
	http.createServer() method creates a server and handle the requests of client.
*/

/*
	res.writeHead() is a method of the response object. add headers which are additional info about our server response. 'Content-Type' pertains to the data type of content response. the 200 is an HTTP status; tells the client about status of request. '200' means OK.

	res.end() is a method of the response object which ends the server response and sends a message as a string
	
	.listen() assign a port to a server. there are several tasks and process in our computer which are also designated their specific ports

	port is a virtual point where connections start and end.
	localhost:4000 -local host is current mahcine, 4000 is the port number assigned.

*/

let http = require("http");

http.createServer(function(req, res) {

	if (req.url === "/") {

		res.writeHead(200, {'Content-Type': `text/plain`});
		res.end('hello world');

	} else if (req.url === "/login") {
		res.writeHead(200, {'Content-Type': `text/plain`});
		res.end('welcome');
	} else {
		res.writeHead(404, {'Content-Type': `text/plain`});
		res.end('resource not found');
	}




}).listen(4000);

console.log("Server is running on localHost:4000");